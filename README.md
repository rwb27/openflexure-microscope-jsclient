# OpenFlexure.vue

## To-do
* ~~Connect~~
    * ~~Connect to user-input IP~~
    * ~~Store connection in vuex store~~

* ~~Move~~
    * ~~Read absolute position~~
    * ~~Change absolute position~~
    * ~~Keyboard movement~~
    * ~~Mouse scroll focus~~
    * ~~Double-click to centre~~

* Gallery
    * ~~Basic card for each capture~~
    * ~~Thumbnails~~
    * ~~Reload button~~
    * ~~Download buttons~~
        * ~~Lightbox~~
        * ~~Save to disk~~ (Redundant by context menu)
    * ~~Basic info/metadata modal~~
    * ~~Tags~~
        * ~~Read tag list~~
        * ~~Add tag button~~
        * ~~Delete tag button~~
    * Tag filter

* ~~Capture~~
    * ~~UI elements for capture options~~
    * ~~Big, friendly capture button~~
    * ~~Metadata for captures~~
    * ~~Tags for captures~~

* Plugins
    * ~~Placeholder for other plugins~~
    * Accordion for:
        * ~~Autofocus~~ (Needs functionality added to placeholders)
        * ~~Recalibrate~~

* Settings
    * Metadata keys to automatically add (e.g. userID, patientID)